namespace Dedicnost;

public class Manager: Worker
{
    public override int CalculateSalary()
    {
        return 200 * WorkingDays();
    }
}