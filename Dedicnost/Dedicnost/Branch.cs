namespace Dedicnost
{
    public class Branch
    {
        public string BranchName { get; set; }

        public Branch(string name)
        {
            BranchName = name;
        }
    }
}