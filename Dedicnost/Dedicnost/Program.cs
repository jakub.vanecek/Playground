﻿namespace Dedicnost
{
    public class Program
    {
        static void Main(string[] args)
        {
            var branchA = new Branch("Portaly");

            var zmest = new Worker("Peter", "Parker");

            var man = new Manager();

            Console.WriteLine(zmest.CalculateSalary());
            Console.WriteLine(man.CalculateSalary());
        }
    }
}
