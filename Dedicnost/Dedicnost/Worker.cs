using System.Reflection;

namespace Dedicnost;

public class Worker
{
    public string Name { get; set; }
    public string Surname { get; set; }

    public Branch Branch { get; set; }

    public Worker()
    {

    }

    public Worker( string name, string surname)
    { 
        Name = name;
        Surname = surname;
    }

    public virtual int CalculateSalary()
    {
        return 100 * WorkingDays();
    }

    protected int WorkingDays()
    {
        return DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
    }
}