﻿using System;

namespace ConsoleApp1
{
    abstract class Program
    {
        
        
        static void Main()
        {
            
            double a;
            double b;
            double c;
            double? vysledek;
            double? vysledek2;

            a = 1;
            b = 0;
            c = 1;

            Vypocet.vypocet(a,b,c,out vysledek, out vysledek2);

            Console.WriteLine(vysledek.HasValue ? Math.Round(vysledek.Value,2) : vysledek);
            Console.WriteLine(vysledek2.HasValue ? Math.Round(vysledek2.Value,2) : vysledek2);


            if(10 <= null)
            {
                Console.WriteLine("hokuspokus");
            }
            else{
                Console.WriteLine("nic");
            }
            
        }

    }
}