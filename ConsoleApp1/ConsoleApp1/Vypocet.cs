using System;

namespace ConsoleApp1
{
    public class Vypocet
    {
        public static void vypocet(double a, double b, double c, out double? x1, out double? x2)
        {
            double d;
            
            d = Math.Pow(b, 2) - 4 * a * c;


            if (d < 0)
            {
                x1 = null;
                x2 = null;
            }
            else if (d == 0)
            {
                x1 = (-b + Math.Sqrt(d)) / 2 * a;
                x2 = null;
            }
            else
            {
                x1 = (-b + Math.Sqrt(d)) / 2 * a;
                x2 = (-b - Math.Sqrt(d)) / 2 * a;
            }
        }
    }
}