using System.ComponentModel;

namespace Pokus;

public enum Moznosti
{
    [Description("Rovnice ma jeden vysledek")] OneResult,
    [Description("Rovnice ma dva vysledky")] TwoResults,
    [Description("Rovnice nema vysledek")] NoResult,
    [Description("Jsou zadany nepravne parametry")] IncorrectInput
}