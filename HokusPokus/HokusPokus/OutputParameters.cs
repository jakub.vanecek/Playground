using Pokus;

public class OutputParameters
{
    public double? x1 {get ;set;}
    public double? x2 {get; set;}
    public Moznosti pocetVysledku { get; set; }
} 