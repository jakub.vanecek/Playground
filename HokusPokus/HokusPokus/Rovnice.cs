
    using Pokus;

    public class rovnice
    {

        public void vypocet(Parameters parameter, OutputParameters output)
        {
            if (parameter.a == 0)
            {
                output.pocetVysledku = Moznosti.IncorrectInput;
            }
            else 
            {
                double d;

                d = Math.Pow(parameter.b.Value, 2) - 4 * parameter.a.Value * parameter.c.Value;
                if (d == 0)
                {
                    output.x1 = (-parameter.b + Math.Sqrt(d)) / 2 * parameter.a;
                    output.pocetVysledku = Moznosti.OneResult;
                }
                else if (d > 0)
                {
                    output.x1 = (-parameter.b + Math.Sqrt(d)) / 2 * parameter.a;
                    output.x2 = (-parameter.b - Math.Sqrt(d)) / 2 * parameter.a;
                    output.pocetVysledku = Moznosti.TwoResults;
                }
                else
                {
                    output.pocetVysledku = Moznosti.NoResult;
                }
            }

            output.x1 = output.x1.HasValue?Math.Round(output.x1.Value, 2): null;
            output.x2 = output.x2.HasValue?Math.Round(output.x2.Value, 2): null;

        }

    }
